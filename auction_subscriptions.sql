CREATE TABLE IF NOT EXISTS auction_subscriptions(
    subscription_id SERIAL PRIMARY KEY,
    uuid TEXT,
    discord_id NUMERIC,
    subscription_type TEXT
);
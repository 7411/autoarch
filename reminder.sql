CREATE TABLE IF NOT EXISTS reminder(
    timestamp NUMERIC PRIMARY KEY,
    user_id NUMERIC,
    channel_id NUMERIC,
    reason TEXT,
);
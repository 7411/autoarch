CREATE TABLE IF NOT EXISTS moderation(
    serverid NUMERIC PRIMARY KEY,
    features JSON,
    custom_filter JSON
)
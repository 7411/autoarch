CREATE TABLE IF NOT EXISTS auctions(
    uuid TEXT PRIMARY KEY,
    num_auctions NUMERIC
);
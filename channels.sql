CREATE TABLE IF NOT EXISTS channels(
    yt_id TEXT PRIMARY KEY,
    channels_json TEXT,
    last_video TEXT
)
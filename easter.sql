CREATE TABLE IF NOT EXISTS easter_servers(
    server_id NUMERIC PRIMARY KEY,
    enabled BOOL
);

CREATE TABLE IF NOT EXISTS easter_inventories(
    user_id NUMERIC PRIMARY KEY,
    inventory JSON
);
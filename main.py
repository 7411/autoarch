import secret
import asyncio
import discord
from discord.ext import commands
from extensions import *
import config

bot = commands.Bot(command_prefix=config.prefix)

bot.load_extension("extensions.help")
bot.load_extension("extensions.handlers")
bot.load_extension("extensions.utils")
bot.load_extension("extensions.minecraft")
bot.load_extension("extensions.image")
bot.load_extension("extensions.crypto")
bot.load_extension("extensions.languages")
bot.load_extension("extensions.youtube")
bot.load_extension("extensions.moderation")
bot.load_extension("extensions.relay_support")
bot.load_extension("extensions.voice")
bot.load_extension("extensions.story")
bot.load_extension("extensions.movies")
bot.load_extension("extensions.reminder")
bot.load_extension("extensions.easter")

bot.run(secret.token)

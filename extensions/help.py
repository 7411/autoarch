import discord
from discord.ext import commands

# Help command taken from previous AutoArch iteration
# Not yet fully integrated

class AutoArchHelpCommand(commands.MinimalHelpCommand):
    def get_command_signature(self, command):
        return '{0.clean_prefix}{1.qualified_name} {1.signature}'.format(self, command)

    async def send_bot_help(self, mapping):
        ctx = self.context
        help_embed = discord.Embed(
            title = "Commands",
            description = self.get_opening_note(),
            color = discord.Color.gold()
        )
        help_embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)
        for cog in mapping.keys():

            name = "Other"
            if cog != None:
                name = cog.qualified_name
                if hasattr(cog, "custom_icon"):
                    name = f"{cog.custom_icon} {name}"

            help_embed.add_field(name = name, value = ", ".join([command.name for command in mapping[cog]]))

        await self.get_destination().send(embed = help_embed)

    async def send_command_help(self, command):
        ctx = self.context
        help_embed = discord.Embed(
            title = self.get_command_signature(command),
            description = command.help,
            color = discord.Color.gold()
        )
        help_embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)

        await self.get_destination().send(embed = help_embed)

    async def send_cog_help(self, cog):
        ctx = self.context
        commands = ", ".join([command.qualified_name for command in cog.get_commands()])
        help_embed = discord.Embed(
            title = cog.qualified_name,
            description = commands,
            color = discord.Color.gold()
        )
        help_embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)

        await self.get_destination().send(embed = help_embed)

    async def send_group_help(self, group):
        ctx = self.context
        commands = ", ".join([command.qualified_name for command in group.commands])
        help_embed = discord.Embed(
            title = f"{group.qualified_name} Subcommands",
            description = commands,
            color = discord.Color.gold()
        )
        help_embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)

        await self.get_destination().send(embed = help_embed)

class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Help"

def setup(bot):
    bot.add_cog(Help(bot))
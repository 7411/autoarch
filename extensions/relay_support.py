import discord
from discord.ext import commands

class RelayCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.bot.process_commands = self.process_commands
    
    async def process_commands(self, message):
        separated = message.content.split(" ")
        if separated[0].startswith("<") and separated[0].endswith(">"):
            del separated[0]
            message.content = " ".join(separated)
        ctx = await self.bot.get_context(message)
        await self.bot.invoke(ctx)

def setup(bot):
    bot.add_cog(RelayCog(bot))
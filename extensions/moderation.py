import discord
from discord.ext import commands
import asyncpg
import secret
import moderation_filters
import json

class Moderation(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.conn = None
        self.servers = {} # key: server ID, item: (features list(int) (crass, crude, religious, allcaps, invites), custom_filter list(str))
    
    # Ensures that the DB connection is open.
    async def check(self):
        if self.conn is None:
            self.conn = await asyncpg.connect(f'postgresql://postgres:{secret.password}@localhost/moderation')
    
    async def get_server_config(self, serverid):
        await self.check()
        if serverid not in list(self.servers.keys()):
            row = await self.conn.fetchrow("SELECT * FROM moderation WHERE serverid=$1", serverid)
            if row is None: # Server not configured
                self.servers[serverid] = ([0,0,0,0,0],[])
                await self.set_server_config(serverid, [0,0,0,0,0],[])
            else:
                self.servers[serverid] = (json.loads(row["features"]), json.loads(row["custom_filter"]))
        return self.servers[serverid]
    
    async def set_server_config(self, serverid, features, custom_filter):
        await self.check()
        row = await self.conn.fetchrow("SELECT * FROM moderation WHERE serverid=$1", serverid)
        if row is None:
            await self.conn.execute("INSERT INTO moderation(serverid, features, custom_filter) VALUES($1, $2, $3)", serverid, json.dumps(features), json.dumps(custom_filter))
        else:
            await self.conn.execute("UPDATE moderation SET features=$1, custom_filter=$2 WHERE serverid=$3", json.dumps(features), json.dumps(custom_filter), serverid)

    async def check_violation(self, message):
        if message.guild is None:
            return False, "DMs"
        serverid = message.guild.id
        config = await self.get_server_config(serverid)
        content = message.content.lower()
        features = config[0]
        custom_filter = config[1]
        if features[0]==1: # Crass filter enabled
            for word in moderation_filters.crass:
                if word in content:
                    return True, "crass"
        if features[1]==1: # Crude filter enabled
            for word in moderation_filters.crude:
                if word in content:
                    return True, "crude"
        if features[2]==1: # Religious filter enabled
            for word in moderation_filters.religious:
                if word in content:
                    return True, "religious"
        if features[3]==1: # Allcaps filter enabled
            if len(content)>4:
                c = message.content
                lower = c.replace("İ","i").lower()
                upper = 0
                for i, character in enumerate(lower):
                    if character != c[i]:
                        upper += 1
                if upper>(len(lower)*(1/2)):
                    return True, "allcaps"

        if features[4]==1: # Invites filter enabled
            if ("discord.gg" in content) or ("discordapp.com/invite" in content):
                return True, "invites"
        
        for word in custom_filter:
            if word in content:
                return True, "custom"
        return False, "acceptable"

    @commands.Cog.listener()
    async def on_message(self, message):
        permissions = message.author.permissions_in(message.channel)
        if permissions.manage_messages:
            return
        if message.author.id == self.bot.user.id:
            return
        try:
            result, reason = await self.check_violation(message)
        except Exception as e:
            await message.channel.send(str(e))
        if result:
            if not message.author.bot:
                await message.channel.send(f"{message.author.mention} Your message violates the {reason} filter.")
            await message.delete()
    
    @commands.has_permissions(manage_messages=True)
    @commands.command()
    async def filter(self, ctx, operation=None, *, item=None):
        try:
            if ctx.guild is None:
                return
            serverid = ctx.guild.id
            config = await self.get_server_config(serverid)
            features = config[0]
            custom_filter = config[1]

            if operation=="add":
                if item is None:
                    await ctx.send("You must provide a word to add to the custom filter.")
                    return
                item = item.lower()
                if "\n" in item:
                    await ctx.send("Custom filters cannot have newlines.")
                for phrase in custom_filter:
                    if phrase.lower() in item:
                        await ctx.send("This custom filter is already covered by another custom filter.")
                        return
                custom_filter.append(item)
                await self.set_server_config(serverid, config[0], custom_filter)
                await ctx.send(f"Successfully added ||`{item}`|| to the filter.")
                return
            
            if operation=="remove":
                if item is None:
                    await ctx.send("You must specify a word to remove from the custom filter.")
                    return
                item = item.lower()
                for phrase in custom_filter:
                    if phrase.lower() == item.lower():
                        custom_filter.remove(phrase)
                        await self.set_server_config(serverid, config[0], custom_filter)
                        await ctx.send(f"Successfully removed `{item}` from the filter.")
                        return
                await ctx.send(f"That word was not found in the custom filters. Use `{ctx.prefix}filter list` to see all custom filters.")
                return
            
            if operation=="list":
                embed = discord.Embed(color=discord.Color.gold())
                embed.title = f"{ctx.guild.name} Custom Filters"
                custom_filter = config[1]
                embed.description = "```\n"+"\n".join(custom_filter)+"\n```"
                await ctx.send(embed=embed)
                return
            
            if operation=="enable":
                if item is None:
                    await ctx.send(f"Please specify a feature to enable. See the available ones using `{ctx.prefix}filter`.")
                    return
                item = item.lower()
                features = config[0]
                if item=="crass":
                    features[0] = 1
                elif item=="crude":
                    features[1] = 1
                elif item=="religious":
                    features[2] = 1
                elif item=="allcaps":
                    features[3] = 1
                elif item=="invites":
                    features[4] = 1
                else:
                    await ctx.send(f"No feature found by that name. See the available ones using `{ctx.prefix}filter`.")
                    return

                await self.set_server_config(serverid, features, config[1])
                await ctx.send("Successfully updated filter.")
                return

            if operation=="disable":
                if item is None:
                    await ctx.send(f"Please specify a feature to disable. See the available ones using `{ctx.prefix}filter`.")
                    return
                item = item.lower()
                features = config[0]
                if item=="crass":
                    features[0] = 0
                elif item=="crude":
                    features[1] = 0
                elif item=="religious":
                    features[2] = 0
                elif item=="allcaps":
                    features[3] = 0
                elif item=="invites":
                    features[4] = 0
                else:
                    await ctx.send(f"No feature found by that name. See the available ones using `{ctx.prefix}filter`.")
                    return

                await self.set_server_config(serverid, features, config[1])
                await ctx.send("Successfully updated filter.")
                return


            embed = discord.Embed(color=discord.Color.gold())
            embed.title = f"{ctx.guild.name} Filter"
            feature_string = "Filter features:\n"
            feature_string += ("\U00002705" if features[0] else "\U0000274c") + f" `crass` ({len(moderation_filters.crass)} phrases)\n"
            feature_string += ("\U00002705" if features[1] else "\U0000274c") + f" `crude` ({len(moderation_filters.crude)} phrases)\n"
            feature_string += ("\U00002705" if features[2] else "\U0000274c") + f" `religious` ({len(moderation_filters.religious)} phrases)\n"
            feature_string += ("\U00002705" if features[3] else "\U0000274c") + f" `allcaps`\n"
            feature_string += ("\U00002705" if features[4] else "\U0000274c") + f" `invites`\n"
            feature_string += f"Custom filter phrases: {len(custom_filter)}\n\n"
            feature_string += f"Use \n`{ctx.prefix}filter [add/remove/list] [custom filter if changing]` and \n`{ctx.prefix}filter [enable/disable] [feature]`\nto configure."
            embed.description = feature_string
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.send(f"An error occurred.\n```\n{e}\n```")


def setup(bot):
    bot.add_cog(Moderation(bot))

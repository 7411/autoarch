import discord
from discord.ext import commands
import asyncpg
import secret
import json
import random

class Easter(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.conn = None
        self.base_inventory = {
            "blue" : 0,
            "red" : 0,
            "dragon" : 0
        }
        self.emojis = {
            "blue" : "<:egg_blue:964596007365148692>",
            "red" : "<:egg_red:964596007155421256>",
            "dragon" : "<:egg_dragon:964596007499337768>"
        }
        self.chances = {
            "blue" : 0.02,
            "red" : 0.01,
            "dragon" : 0.004
        }
        self.ignore_bots = True

    # Ensures that the DB connection is open.
    async def check(self):
        if self.conn is None:
            self.conn = await asyncpg.connect(f'postgresql://postgres:{secret.password}@localhost')
    
    async def get_user_inventory(self, userid):
        await self.check()
        row = await self.conn.fetchrow("SELECT * FROM easter_inventories WHERE user_id=$1", userid)
        if row is None:
            inventory = json.dumps(self.base_inventory)
            await self.conn.execute("INSERT INTO easter_inventories(user_id, inventory) VALUES($1, $2)", userid, inventory)
            return self.base_inventory
        return json.loads(row["inventory"])
    
    async def set_user_inventory(self, userid, inventory):
        await self.check()
        inventory_string = json.dumps(inventory)
        row = await self.conn.fetchrow("SELECT * FROM easter_inventories WHERE user_id=$1", userid)
        if row is None:
            await self.conn.execute("INSERT INTO easter_inventories(user_id, inventory) VALUES($1, $2)", userid, inventory_string)
            return
        await self.conn.execute("UPDATE easter_inventories SET inventory=$1 WHERE user_id=$2", inventory_string, userid)
    
    async def award_user_egg(self, userid, egg):
        inventory = await self.get_user_inventory(userid)
        current_egg_count = inventory[egg] or 0
        inventory[egg] = current_egg_count + 1
        await self.set_user_inventory(userid, inventory)
    
    async def is_server_participating(self, serverid):
        await self.check()
        row = await self.conn.fetchrow("SELECT * FROM easter_servers WHERE server_id=$1", serverid)
        if row is None:
            return False
        return row["enabled"]
    
    async def set_server_participation(self, serverid, status):
        await self.check()
        row = await self.conn.fetchrow("SELECT * FROM easter_servers WHERE server_id=$1", serverid)
        if row is None:
            await self.conn.execute("INSERT INTO easter_servers(server_id, enabled) VALUES($1, $2)", serverid, status)
            return
        await self.conn.execute("UPDATE easter_servers SET enabled=$1 WHERE server_id=$2", status, serverid)
    
    @commands.command(help="See all the easter eggs you've found!")
    async def egghunt(self, ctx, user:discord.User=None):
        try:
            if not user:
                user = ctx.author

            inventory = await self.get_user_inventory(user.id)

            description = "\n".join([f"{self.emojis[egg]} {egg.capitalize()} Eggs: {inventory[egg]}" for egg in inventory.keys()])

            embed = discord.Embed(
                title = f"{user.display_name}'s Egg Hunt Spoils",
                description = description,
                color = discord.Color.gold()
            )

            await ctx.send(embed=embed)

        except Exception as e:
            await ctx.send(f"{type(e)} exception occurred:\n```\n{e}\n```")

    @commands.has_permissions(manage_messages=True)
    @commands.command(help="Enable the Easter egg hunt in your server!")
    async def enable_egghunt(self, ctx):
        await self.set_server_participation(ctx.guild.id, True)
        await ctx.send("Let the hunt begin! *[Egghunt Enabled]*")
    
    @commands.has_permissions(manage_messages=True)
    @commands.command(help="Disable the easter egg hunt in your server.")
    async def disable_egghunt(self, ctx):
        await self.set_server_participation(ctx.guild.id, False)
        await ctx.send("'Till next time! *[Egghunt Disabled]*")
    
    @commands.command(help="Debug if the server has the egg hunt enabled")
    async def egghunt_debug(self, ctx):
        try:
            await ctx.send(await self.is_server_participating(ctx.guild.id))
        except Exception as e:
            await ctx.send(f"{type(e)} {e}")
    
    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot and self.ignore_bots:
            return
        
        if (await self.is_server_participating(message.guild.id)):
            for egg in self.chances.keys():
                if random.random() < self.chances[egg]:
                    try:
                        await self.award_user_egg(message.author.id, egg)
                    except Exception as e:
                        await message.channel.send(f"{type(e)}, {e}")
                    await message.channel.send(f"{message.author.display_name} found a {self.emojis[egg]} {egg.capitalize()} Egg!")

def setup(bot):
    bot.add_cog(Easter(bot))
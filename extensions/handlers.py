import discord
from discord.ext import commands

class Handlers(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Handlers"

    @commands.is_owner()
    @commands.command(help = "Reloads a given extension, or all of them.")
    async def reload(self, ctx, extension=None):
        try:
            if extension is None:
                message = await ctx.send("Reloading all extensions...")
                for ext in list(self.bot.extensions).copy():
                    self.bot.reload_extension(ext)
                await message.edit(content=f"Successfully reloaded {len(self.bot.extensions)} extensions")
                return
            self.bot.reload_extension(extension)
            await ctx.send(f"Reloaded extension `{extension}`.")
        except Exception as e:
            await ctx.send(f"An error occured.\n```\n{e}\n```")

    @commands.is_owner()
    @commands.command(help = "Lists loaded extensions.")
    async def extensions(self, ctx):
        ext = "\n".join(ctx.bot.extensions)
        await ctx.send(f"Loaded extensions:\n```\n{ext}\n```")
    
    @commands.is_owner()
    @commands.command(help = "Loads a given extension.")
    async def load(self, ctx, extension):
        try:
            await ctx.send("Loading extension...")
            self.bot.load_extension(extension)
            await ctx.send(f"Successfully loaded extension `{extension}`.")
        except Exception as e:
            await ctx.send(f"An error occured.\n```\n{e}\n```")
    
    @commands.is_owner()
    @commands.command(help = "Unloads a given extension.")
    async def unload(self, ctx, extension):
        try:
            message = await ctx.send("Unloading extension...")
            self.bot.unload_extension(extension)
            await message.edit(content=f"Successfully unloaded extension `{extension}`.")
        except Exception as e:
            await ctx.send(f"An error occured.\n```\n{e}\n```")
            
    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        print(type(error), error)
        if isinstance(error, commands.errors.NotOwner):
            await ctx.send("Owner-only command.")


def setup(bot):
    bot.add_cog(Handlers(bot))
import aiosqlite
import discord
from discord.ext import (commands, tasks)
import aiohttp
import asyncio
import json

class YouTube(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.channel_delay = 20 # channels are each polled 20 seconds apart
        if not self.poll.is_running():
            self.poll.start()

    def cog_unload(self):
        self.poll.cancel()

    async def get_latest_video(self, ytchannel):
        async with aiohttp.ClientSession() as session:
            async with session.get(f"https://youtube.com/channel/{ytchannel}/videos") as resp:
                if not resp.status == 200:
                    async with session.get(f"https://youtube.com/c/{ytchannel}/videos") as r:
                        if not r.status == 200:
                            return None # Channel does not exist
                        html = await r.text()
                        
                        last_video = ""

                        index = html.find('watch?v=')
                        end = html.find("\"", index+10, index+100)
                        if index != -1 and end != -1:
                            last_video = html[index+8:end]
                        
                        return last_video

                html = await resp.text()
                
                last_video = ""

                index = html.find('watch?v=')
                end = html.find("\"", index+10, index+100)
                if index != -1 and end != -1:
                    last_video = html[index+8:end]
                
                return last_video

    @tasks.loop(seconds=600)
    async def poll(self):
        async with aiosqlite.connect("channels.sqlite") as db:
            async with db.execute("SELECT * FROM channels") as cursor:
                async for row in cursor:
                    yt_id = row[0]
                    previous_vid = row[2]
                    new_vid = await self.get_latest_video(yt_id)
                    if previous_vid != new_vid and not (new_vid is None):
                        channel_ids = json.loads(row[1])
                        for cid in channel_ids:
                            channel = self.bot.get_channel(cid)
                            if channel is None:
                                channel = await self.bot.fetch_channel(cid)
                            await channel.send(f"https://youtube.com/watch?v={new_vid}")
                        await db.execute("UPDATE channels SET last_video=$1 WHERE yt_id=$2", (new_vid, yt_id))
                        await db.commit()
                    await asyncio.sleep(self.channel_delay)
    
    @commands.is_owner()
    @commands.command()
    async def inspect_db(self, ctx):
        async with aiosqlite.connect("channels.sqlite") as db:
            async with db.execute("SELECT * FROM channels") as cursor:
                async for row in cursor:
                    await ctx.send(str(row))
    
    @commands.command(help="Links a specific YouTube channel to get updates.")
    @commands.has_permissions(manage_messages=True)
    async def link_channel(self, ctx, ytchannel, channel:discord.TextChannel=None):
        if "/" in ytchannel:
            await ctx.send("Please use a valid YouTube channel ID.")
            return
        if channel is None:
            channel = ctx.channel
        
        if channel.guild.id != ctx.message.guild.id:
            await ctx.send("Please use a channel ID corresponding to a channel in this server.")
            return
        try:
            vid = await self.get_latest_video(ytchannel)
            if vid is None:
                await ctx.send("Please use a valid YouTube channel ID.")
                return
            async with aiosqlite.connect("channels.sqlite") as db:
                async with db.execute("SELECT * FROM channels WHERE yt_id=$1", (ytchannel,)) as cursor:
                    exists=False
                    current_channels = []
                    last_vid = ""
                    async for row in cursor:
                        exists=True
                        current_channels = json.loads(row[1])
                        last_vid = row[2]
                    if last_vid == "":
                        last_vid = vid
                    if channel.id in current_channels:
                        await ctx.send(f"A feed for this channel already exists in {channel.mention}.")
                        return
                    current_channels.append(channel.id)
                    channels_json = json.dumps(current_channels)
                    if not exists:
                        await db.execute("INSERT INTO channels(yt_id, channels_json, last_video) VALUES($1, $2, $3)", (ytchannel, channels_json, last_vid))
                    else:
                        await db.execute("UPDATE channels SET channels_json=$1 WHERE yt_id=$2", (channels_json, ytchannel))
                    await db.commit()
                    await ctx.send("YouTube feed linked!")

        except Exception as e:
            await ctx.send(f"An error has occurred.\n```\n{e}\n```")
    
    @commands.command(help="Unlinks all YouTube feeds in a Discord channel.")
    @commands.has_permissions(manage_messages=True)
    async def unlink(self, ctx, channel:discord.TextChannel=None):
        if channel is None:
            channel = ctx.channel
        await ctx.send(f"Unlink all YouTube channel feeds in the channel {channel.mention}?\nRespond with \"yes\" to confirm.")
        def check(message):
            return message.channel.id == ctx.channel.id and message.author.id == ctx.author.id
        try:
            response = await self.bot.wait_for("message", check=check)
            if response.content.lower() == "yes":
                await ctx.send("Unlinking all feeds.")
                async with aiosqlite.connect("channels.sqlite") as db:
                    async with db.execute("SELECT * FROM channels") as cursor:
                        async for row in cursor:
                            ytid = row[0]
                            current_channels = json.loads(row[1])
                            if channel.id not in current_channels:
                                continue
                            current_channels.remove(channel.id)
                            channels_json = json.dumps(current_channels)
                            await db.execute("UPDATE channels SET channels_json=$1 WHERE yt_id=$2", (channels_json, ytid))
                            await db.commit()
                        await ctx.send("All feeds successfully unlinked.")
            else:
                await ctx.send("Confirmation not acquired, aborting.")
                return
        except Exception as e:
            await ctx.send(f"Confirmation not acquired, aborting.\n```\n{e}\n```")
            return

def setup(bot):
    bot.add_cog(YouTube(bot))
import discord
from discord.ext import commands
import asyncio

class Voice(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.morse_code = {
            "A" : ("A.ogg", ".-"),
            "B" : ("B.ogg", "-..."),
            "C" : ("C.ogg", "-.-."),
            "D" : ("D.ogg", "-.."),
            "E" : ("E.ogg", "."),
            "F" : ("F.ogg", "..-."),
            "G" : ("G.ogg", "--."),
            "H" : ("H.ogg", "...."),
            "I" : ("I.ogg", ".."),
            "J" : ("J.ogg", ".---"),
            "K" : ("K.ogg", "-.-"),
            "L" : ("L.ogg", ".-.."),
            "M" : ("M.ogg", "--"),
            "N" : ("N.ogg", "-."),
            "O" : ("O.ogg", "---"),
            "P" : ("P.ogg", ".--."),
            "Q" : ("Q.ogg", "--.-"),
            "R" : ("R.ogg", ".-."),
            "S" : ("S.ogg", "..."),
            "T" : ("T.ogg", "-"),
            "U" : ("U.ogg", "..-"),
            "V" : ("V.ogg", "...-"),
            "W" : ("W.ogg", ".--"),
            "X" : ("X.ogg", "-..-"),
            "Y" : ("Y.ogg", "-.--"),
            "Z" : ("Z.ogg", "--.."),
            " " : ("space.ogg", "  ")
        }
        self.max_speech_length = 50
    
    def get_voice_client(self, ctx):
        if ctx.guild is None:
            return None
        return ctx.guild.voice_client

    @commands.command(help = "Joins the bot to the user's voice channel.")
    async def join(self, ctx):
        try:
            if ctx.guild is None:
                await ctx.send("This command cannot be used in DMs.")
                return
            voicestate = ctx.author.voice
            if voicestate is None:
                await ctx.send("You are not connected to a voice channel.")
                return
            channel = voicestate.channel
            await channel.connect()
            await ctx.send("Joined!")
        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.command(help = "Makes the bot leave the voice channel it's connected to.")
    async def leave(self, ctx):
        voice_client = self.get_voice_client(ctx)
        if voice_client is None:
            await ctx.send("Nothing could be done.")
        await voice_client.disconnect()
        await ctx.send("Done!")
    
    @commands.command(help = "Allows translation, synthesis, and alphabet practice")
    async def morse(self, ctx, *, args=None):
        try:
            if args is None:
                embed = discord.Embed(title="International Morse Code")
                embed.description = "This command is intended to help you use Morse code."
                embed.description += "\nUse the following arguments to start:"
                embed.description += f"\n`{ctx.prefix}morse encode <text>` - Encode a phrase (and speak it in VC, if connected.)"
                embed.description += f"\n`{ctx.prefix}morse decode <morse>` - Decode existing Morse code."
                embed.color = discord.Color.gold()
                embed.set_image(url="https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/Morse-Code.svg/558px-Morse-Code.svg.png")
                await ctx.send(embed=embed)
                return
            cmds = args.split(" ")
            operation = cmds[0]
            args = args[len(operation)+1:]
            if operation == "encode":
                text = args.upper()
                result = ""
                encoded = ""
                for character in text:
                    if character in self.morse_code.keys():
                        result += character
                        encoded += f"{self.morse_code[character][1]} "
                
                await ctx.send(f"Result:\n```\n{encoded}\n```")

                voice = self.get_voice_client(ctx)
                if voice is None:
                    return

                if len(result) > self.max_speech_length:
                    await ctx.send(f"Cannot synthesize audio - maximum spoken Morse characters exceeded ({len(result)}/{self.max_speech_length})")
                    return
                if voice.is_playing():
                    voice.stop()
                for character in result:
                    while True:
                        if voice.is_playing():
                            await asyncio.sleep(0.1)
                            continue
                        source = discord.FFmpegPCMAudio("morse/"+self.morse_code[character][0])
                        voice.play(source)
                        break
                return
            
            if operation == "decode":
                args = args.replace("  ", " 2SPACE ")
                letters = args.split(" ")
                for key in self.morse_code.keys():
                    for i, letter in enumerate(letters):
                        if letter == self.morse_code[key][1]:
                            letters[i] = key
                        if letter == "2SPACE":
                            letters[i] = " "
                result = "".join(letters)
                
                await ctx.send(f"Result:\n```\n{result}\n```")
                return

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")

def setup(bot):
    bot.add_cog(Voice(bot))
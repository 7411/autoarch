import discord
from discord.ext import commands
from pycoingecko import CoinGeckoAPI
from urllib.parse import quote
import json
import asyncpg
from datetime import datetime
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
import aiohttp
from colorhash import ColorHash
import secret

class Crypto(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Cryptocurrency"
        self.cg = CoinGeckoAPI()
        self.conn = None
        self.portfolio_currency = "usd"

    # Ensures that the DB connection is open.
    async def check(self):
        if self.conn is None:
            self.conn = await asyncpg.connect(f'postgresql://postgres:{secret.password}@localhost/crypto')
    
    # Convenience wrapper method to access the CoinGecko API asynchronously.
    async def get_prices(self, cryptos, currency):
        def fetch_prices(c, f):
            return self.cg.get_price(ids=c, vs_currencies=f)

        return await self.bot.loop.run_in_executor(None, fetch_prices, cryptos, currency)

    @commands.Cog.listener()
    async def on_ready(self):
        await self.check()


    @commands.command(help = "Converts cryptocurrency values to other currencies, via the CoinGecko API")
    async def convert(self, ctx, *, input):
        data = input.split(" ") # TODO: fix this madness, what kind of parsing is this
        try:
            value = float(data[0])
            crypto = quote(data[1].lower())
            fiat = quote(data[3].lower())
            
            price = await self.get_prices(crypto, fiat)

            data = price[list(price.keys())[0]]

            price = float(data[list(data.keys())[0]] * value)

            formatted_price = "{:.10f}".format(price)

            await ctx.send(f"{value} {crypto} is {formatted_price} {fiat.upper()}")

        except Exception as e:
            await ctx.send(f"An error occurred. \n```\n{e}\n```\nEnsure you are using the correct currency identifiers.")
    
    @commands.command(help = "Views your crypto portfolio, using data from the CoinGecko API.", aliases=["portfolio"])
    async def crypto(self, ctx, user:discord.User=None):
        try:
            await self.check()
            if user is None:
                user = ctx.author
            userid = str(user.id)

            record = await self.conn.fetchrow("SELECT * FROM portfolios WHERE userid = $1", userid)

            if record is None:
                await ctx.send(f"This user does not currently have a portfolio. See `{self.bot.command_prefix}help setcrypto` to begin.")
                return

            portfolio = json.loads(record["portfolio"])

            formatted = "\n".join([f"{portfolio[key]} **{key}**" for key in portfolio.keys()])

            embed = discord.Embed(color=discord.Color.gold())

            embed.title = f"{user.display_name}'s Portfolio"

            # less-than-clean method to ensure correct grammar
            if user.display_name[-1].lower() == "s":
                embed.title = f"{user.display_name}' Portfolio"

            embed.set_thumbnail(url=user.avatar_url)

            embed.description = formatted

            embed.set_footer(text="Prices obtained via the CoinGecko API.")

            embed.timestamp = datetime.now()

            message = await ctx.send(embed=embed)

            if len(list(portfolio.keys())) == 0:
                return

            def get_prices(portfolio):
                cryptos = list(portfolio.keys())
                data = self.cg.get_price(ids=cryptos, vs_currencies=self.portfolio_currency)
                return data
            
            data = await self.bot.loop.run_in_executor(None, get_prices, portfolio)

            total = 0
            formatted = ""

            for coin in portfolio.keys():
                value = data[coin][self.portfolio_currency]
                holdings = value*portfolio[coin]
                formatted += f"`{coin}`: "
                formatted += "**{:0.2f} USD**\n".format(holdings)
                formatted += f"`{round(value, 10)}` USD per {coin}\n\n"
                total += holdings
            
            formatted += "**Total: {:0.2f} USD**".format(total)
            embed.description = formatted
            await message.edit(embed=embed)

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.command(help = "Set the amount of a cryptocurrency in your portfolio.", aliases = ["set"])
    async def setcrypto(self, ctx, crypto, number):
        try:
            amount = float(number)
            await self.check()
            if amount < 0:
                await ctx.send("You cannot set a negative balance!")
                return
            
            # TODO: Implement identifier lookup using cached DB of cryptocurrency identifiers 
            # using /coins/list CoinGecko API endpoint
            crypto = quote(crypto.lower())
            
            price = 0

            try:
                price = await self.get_prices(crypto, self.portfolio_currency)
                crypto = list(price.keys())[0]

            except Exception:
                await ctx.send("No cryptocurrency found by that name. Identifier lookups are not yet implemented.")
                return
            
            crypto = list(price.keys())[0]
            
            userid = str(ctx.author.id)

            record = await self.conn.fetchrow("SELECT * FROM portfolios WHERE userid = $1", userid)

            if record is None: # User has no portfolio
                portfolio = {
                    crypto : amount
                }
                portfolio
                await self.conn.execute("INSERT INTO portfolios(userid, portfolio) VALUES ($1, $2)", userid, json.dumps(portfolio))
                await ctx.send("Portfolio created and updated.")
                return
            
            # Portfolio does exist

            portfolio = json.loads(record["portfolio"])
            portfolio[crypto] = amount
            
            if amount == 0:
                del portfolio[crypto]
            
            await self.conn.execute("UPDATE portfolios SET portfolio=$1 WHERE userid=$2", json.dumps(portfolio), userid)

            await ctx.send("Portfolio updated.")

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")

    @commands.is_owner()
    @commands.command()
    async def force_set(self, ctx, userid, crypto, amount:float, keep:int=0):
        try:
            await self.check()
            record = await self.conn.fetchrow("SELECT * FROM portfolios WHERE userid = $1", userid)

            if record is None: # User has no portfolio
                portfolio = {
                    crypto : amount
                }
                portfolio
                await self.conn.execute("INSERT INTO portfolios(userid, portfolio) VALUES ($1, $2)", userid, json.dumps(portfolio))
                await ctx.send("Portfolio created and updated.")
                return
            
            # Portfolio does exist

            portfolio = json.loads(record["portfolio"])
            portfolio[crypto] = amount

            if amount == 0 and keep == 0:
                del portfolio[crypto]
            
            await self.conn.execute("UPDATE portfolios SET portfolio=$1 WHERE userid=$2", json.dumps(portfolio), userid)

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.command()
    async def graph(self, ctx, user:discord.User=None):

        if user is None:
            user = ctx.author
        
        try:
            await self.check()

            msg = await ctx.send("Loading...")

            avatar_url = user.avatar_url_as(format="png")

            async with aiohttp.ClientSession() as session:
                async with session.get(str(avatar_url)) as resp:
                    pfp_bytes = BytesIO(await resp.read())
                    pfp_bytes.seek(0)

                    record = await self.conn.fetchrow("SELECT * FROM portfolios WHERE userid = $1", str(user.id))

                    if record is None:
                        await ctx.send(f"This user does not currently have a portfolio. See `{self.bot.command_prefix}help setcrypto` to begin.")
                        return

                    portfolio = json.loads(record["portfolio"])

                    def get_prices(portfolio):
                        cryptos = list(portfolio.keys())
                        data = self.cg.get_price(ids=cryptos, vs_currencies=self.portfolio_currency)
                        return data
                    
                    data = await self.bot.loop.run_in_executor(None, get_prices, portfolio)

                    holdings = {}

                    total = 0

                    for coin in portfolio.keys():
                        value = data[coin][self.portfolio_currency] * portfolio[coin]
                        holdings[coin] = value
                        total += value
                    
                    arcs = {}

                    for coin in portfolio.keys():
                        length = (holdings[coin] / total) * 360
                        arcs[coin] = length

                    def get_graph(pfp_bytes, total, arcs):
                        
                        img = Image.open("graph.png")

                        pfp = Image.open(pfp_bytes)

                        pfp = pfp.resize((162,161))

                        img.paste(pfp, (0,0,162,161))

                        draw = ImageDraw.Draw(img)

                        box = (37, 209, 387, 559)

                        angle = 0

                        font = ImageFont.truetype("URWGothic-Book.otf", 25)

                        title_font = ImageFont.truetype("URWGothic-Book.otf", 50)

                        index = 0

                        for crypto in arcs.keys():
                            color = ColorHash(crypto).rgb
                            length = arcs[crypto]

                            draw.pieslice(box, angle, angle+length, color)
                            
                            spacing = 50
                            size = 30

                            pos = 230 + spacing*index

                            x = 470

                            draw.rectangle((x, pos, x+size, pos+size), color)
                            draw.text((x+40, pos+3), crypto, (255,255,255), font)

                            angle += length
                            index += 1
                        
                        draw.text((230,70),"{:0.2f}".format(total), (255,255,255), title_font)

                        res_bytes = BytesIO()

                        img.save(res_bytes, "PNG")

                        res_bytes.seek(0)

                        return res_bytes
                    
                    upload_bytes = await self.bot.loop.run_in_executor(None, get_graph, pfp_bytes, total, arcs)
                    upload_bytes.seek(0)

                    upload_file = discord.File(upload_bytes, filename="graph.png")

                    await ctx.send(file=upload_file)

                    await msg.edit(content="Done!")
        
        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")



def setup(bot):
    bot.add_cog(Crypto(bot))
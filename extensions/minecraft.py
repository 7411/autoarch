import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import aiohttp
import asyncio
from urllib.parse import quote
import secret
import base64
import json
from io import BytesIO
from PIL import Image
import time
import asyncpg

async def get_player_uuid(playername):
    if len(playername) > 0 and len(playername) <= 30:
        playername = quote(playername)
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(f"https://api.mojang.com/users/profiles/minecraft/{playername}") as resp:
                        data = await resp.json()
                        uuid = data["id"]
                        return uuid
            except:
                pass
    return None

async def get_uuid_auctions(uuid):
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(f"https://api.hypixel.net/skyblock/auction?key={secret.hypixel}&player={uuid}") as resp:
                data = await resp.json()
                auctions = data["auctions"]
                actual_auctions = []
                current_time = time.time()
                for auction in auctions:
                    if round(auction["end"]/1000) >= current_time:
                        actual_auctions.append(auction)
                return actual_auctions
        except:
            return []

class Minecraft(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Minecraft"
        self.pool = None
    
    async def ensure_pool(self):
        if not self.pool:
            self.pool = await asyncpg.create_pool(f"postgresql://postgres:{secret.password}@localhost")
    
    @commands.cooldown(2, 1, BucketType.default)
    @commands.command(help = "Gets the UUID of a Minecraft player")
    async def uuid(self, ctx, playername=None):
        if playername is None:
            playername = ctx.author.display_name
        
        uuid = await get_player_uuid(playername)

        if not uuid:
            await ctx.send("No such player exists.")
            return
        
        await ctx.send(f"{playername}'s UUID is `{uuid}`")
    
    @commands.cooldown(2, 1, BucketType.default)
    @commands.command(help = "Checks a player's Skyblock auctions")
    async def auctions(self, ctx, playername=None):
        try:
            if playername is None:
                playername = ctx.author.display_name
            
            uuid = await get_player_uuid(playername)

            if not uuid:
                await ctx.send(f"I couldn't find a Minecraft player named `{playername}`")
                return
            
            auctions = await get_uuid_auctions(uuid)
            
            num_auctions = len(auctions)
            
            auctions_list = "\n".join([auction["item_name"] + " (Ending <t:"+str(round(auction["end"]/1000))+":R>)" for auction in auctions])[0:1000]

            message = f"**{playername} has {num_auctions} auction(s).**\n\n{auctions_list}"

            await ctx.send(message)
        
        except Exception as e:
            await ctx.send(str(e))

    @commands.cooldown(2, 1, BucketType.default)
    @commands.command(help = "Shows a preview of a Minecraft player's face")
    async def face(self, ctx, playername=None):
        if playername is None:
            playername = ctx.author.display_name
        
        uuid = await get_player_uuid(playername)

        if not uuid:
            await ctx.send("No such player exists.")
            return

        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(f"https://sessionserver.mojang.com/session/minecraft/profile/{uuid}") as mojang_resp:
                    mojang_profile = await mojang_resp.json()
                    detailed_info = json.loads(base64.b64decode(mojang_profile["properties"][0]["value"]))
                    skin_url = detailed_info["textures"]["SKIN"]["url"]

                    await ctx.message.add_reaction("\U0001f503")

                    async with session.get(skin_url) as skin_resp:
                        if int(skin_resp.headers["Content-Length"]) > 5000000:
                            return
                        skin_bytes = BytesIO(await skin_resp.read())
                        skin_bytes.seek(0)

                        def get_face(skin):
                            img = Image.open(skin)
                            overlay = img.crop((40, 8, 48, 16))
                            crop = img.crop((8, 8, 16, 16))
                            crop.paste(overlay, mask=overlay)
                            final = crop.resize((100,100), Image.NEAREST)
                            return_bytes = BytesIO()
                            final.save(return_bytes, format="PNG")
                            return return_bytes
                        
                        skin_bytes = await self.bot.loop.run_in_executor(None, get_face, skin_bytes)
                        skin_bytes.seek(0)

                        upload_file = discord.File(skin_bytes, filename="skin.png")
                        await ctx.send(file=upload_file)
                        return
            
        except Exception as e:
            await ctx.send(f"An error occurred while generating the skin preview. \n```\n{e}\n```")

    @commands.cooldown(2, 1, BucketType.default)
    @commands.command(help = "Shows a preview of a Minecraft player's face")
    async def skin(self, ctx, playername=None):
        if playername is None:
            playername = ctx.author.display_name
        
        uuid = await get_player_uuid(playername)

        if not uuid:
            await ctx.send("No such player exists.")
            return

        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(f"https://sessionserver.mojang.com/session/minecraft/profile/{uuid}") as mojang_resp:
                    mojang_profile = await mojang_resp.json()
                    detailed_info = json.loads(base64.b64decode(mojang_profile["properties"][0]["value"]))
                    skin_url = detailed_info["textures"]["SKIN"]["url"]
                    await ctx.send(skin_url)
            
        except Exception as e:
            await ctx.send(f"An error occurred while generating the skin preview. \n```\n{e}\n```")

    @commands.cooldown(2, 1, BucketType.default)
    @commands.command(help = "Looks up a player's Skyblock profile")
    async def skyblock(self, ctx, playername=None):
        if playername is None:
            playername = ctx.author.display_name
        if len(playername) > 30:
            await ctx.send("Playername too long!")
            return
        message = await ctx.send("Fetching Skyblock player info...")
        playername = quote(playername)
        uuid = ""
        # Get player UUID
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(f"https://api.mojang.com/users/profiles/minecraft/{playername}") as resp:
                    data = await resp.json()
                    uuid = data["id"]
                    if len(uuid) == 0:
                        await message.edit(content="Could not fetch player UUID (returned blank)")
                        return

            except Exception as e:
                await message.edit(content=f"I couldn't find a Minecraft player named `{playername}`")
                print(e)
                return

            try:
                async with session.get(f"https://api.hypixel.net/skyblock/profiles?key={secret.hypixel}&uuid={uuid}") as resp:
                    data = await resp.json()
                    if not data["success"]:
                        await message.edit(content="Failed to fetch player Skyblock info (API returned unsuccessfully)")
                        return
                    profiles = data["profiles"]
                    members = data["profiles"][0]["members"]
                    multiple_profiles = False
                    if len(profiles) > 1:
                        multiple_profiles = True
                        highest = profiles[0]
                        score = 0
                        for profile in profiles:
                            for player in profile["members"].items():
                                if "objectives" not in player[1].keys():
                                    continue
                                if len(player[1]["objectives"]) > score:
                                    highest = profile
                                    score = len(player[1]["objectives"])
                        members = highest["members"]


                    member = members[list(members.keys())[0]]
                    stats = member["stats"]
                    objectives = member["objectives"]
                    quests = member["quests"]
                    stat_keys = list(stats.keys())
                    coins = round(member["coin_purse"], 1)
                    coin_purse = f"{coins:,}"
                    crit =  round(stats["highest_critical_damage"], 1) if "highest_critical_damage" in stat_keys else 0
                    highest_crit = f"{crit:,}"
                    kills = int(stats["kills"]) if "kills" in stat_keys else 0
                    deaths = int(stats["deaths"]) if "deaths" in stat_keys else 0
                    auctions_won = int(stats["auctions_won"]) if "auctions_won" in stat_keys else 0
                    auctions_bids = int(stats["auctions_bids"]) if "auctions_bids" in stat_keys else 0
                    auctions_created = int(stats["auctions_created"]) if "auctions_created" in stat_keys else 0
                    auctions_gold = stats["auctions_gold_spent"] if "auctions_gold_spent" in stat_keys else 0
                    auctions_gold_spent = f"{auctions_gold:,}"
                    gifts_given = int(stats["gifts_given"]) if "gifts_given" in stat_keys else 0
                    gifts_received = int(stats["gifts_received"]) if "gifts_received" in stat_keys else 0
                    content = f"""
Skyblock stats for {playername}:
**Highest critical damage dealt:** `{highest_crit}`
**Kills/Deaths:** `{kills}`/`{deaths}`
**Coins in purse:** `{coin_purse}`
**Auctions Won:** `{auctions_won}`/`{auctions_bids}`
**Auctions Created:** `{auctions_created}`
**Gold spent on auctions:** `{auctions_gold_spent}`
**Gifts Given/Received:** `{gifts_given}`/`{gifts_received}`
**Objectives Completed:** `{len(objectives)}`
**Quests Completed:** `{len(quests)}`
                    """

                    pet = "This user has no active pet."

                    if "pets" in member.keys():
                        pets = member["pets"]
                        for item in pets:
                            if item["active"]:
                                pet_type = item["type"].lower().replace("_", " ")
                                pet_rarity = item["tier"].lower()
                                pet = f"**{pet_type}** [{pet_rarity}]".title()
                        pet += f"\n*{len(pets)} total pets owned*"

                    if multiple_profiles:
                        content = f"*Of this player's {len(profiles)} Skyblock profiles, the one with the most objectives complete is shown.*" + content

                    embed = discord.Embed(description=content, colour=discord.Colour.gold())

                    embed.add_field(name="Active Pet", value=pet)

                    embed.set_footer(text="Disclaimer: Users can hide parts of their profile from the API.")

                    try:
                        async with session.get(f"https://sessionserver.mojang.com/session/minecraft/profile/{uuid}") as mojang_resp:
                            mojang_profile = await mojang_resp.json()
                            detailed_info = json.loads(base64.b64decode(mojang_profile["properties"][0]["value"]))
                            skin_url = detailed_info["textures"]["SKIN"]["url"]

                            await message.edit(content="Generating preview...")

                            async with session.get(skin_url) as skin_resp:
                                if int(skin_resp.headers["Content-Length"]) > 5000000:
                                    return
                                skin_bytes = BytesIO(await skin_resp.read())
                                skin_bytes.seek(0)

                                def get_face(skin):
                                    img = Image.open(skin)
                                    overlay = img.crop((40, 8, 48, 16))
                                    crop = img.crop((8, 8, 16, 16))
                                    crop.paste(overlay, mask=overlay)
                                    final = crop.resize((100,100), Image.NEAREST)
                                    return_bytes = BytesIO()
                                    final.save(return_bytes, format="PNG")
                                    return return_bytes
                                
                                skin_bytes = await self.bot.loop.run_in_executor(None, get_face, skin_bytes)
                                skin_bytes.seek(0)

                                upload_file = discord.File(skin_bytes, filename="skin.png")
                                embed.set_thumbnail(url="attachment://skin.png")
                                await ctx.send(file=upload_file, embed=embed)
                                return
                        
                    except Exception as e:
                        await ctx.send(f"An error occurred while generating the skin preview. \n```\n{e}\n```")

                    upload_file = discord.File("could_not_generate.png", filename="failed.png")
                    embed.set_thumbnail(url="attachment://failed.png")
                    await ctx.send(embed=embed, file=upload_file)

            except Exception as e:
                await message.edit(content=f"I couldn't find a Skyblock profile for that player.")
                return


def setup(bot):
    bot.add_cog(Minecraft(bot))
import discord
from discord.ext import commands
import random


class Movies(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        with open("movies.txt", "r") as f:
            self.movies = [[y.replace("\n", "").strip() for y in x.split(";")] for x in f.readlines()]
    
    @commands.command(help = "Returns a random movie from archfan's DVD collection")
    async def movie(self, ctx):
        try:
            choice = random.choice(self.movies)

            description = f"While *{choice[0]}* is in archfan's DVD collection, he hasn't watched it and thus can't comment."

            if choice[1] == "1":
                if choice[2] == "0":
                    description = f"archfan has watched *{choice[0]}* but cannot recommend it."
                if choice[2] == "1":
                    description = f"archfan did enjoy watching *{choice[0]}*."
                if choice[2] == "3":
                    description = f"archfan enjoyed watching *{choice[0]}*, but believes it is best enjoyed (perhaps even only watchable) with a filter."
                if choice[2] == "4":
                    description = f"archfan did watch *{choice[0]}*, but feels he can neither speak for or against it."
            
            if choice[1] == "3":
                description = f"archfan has only watched a part of *{choice[0]}*, but based on what he's seen, "
                if choice[2] == "0":
                    description += "he cannot recommend it."
                if choice[2] == "1":
                    description += "he does recommend it."
                if choice[2] == "3":
                    description += "he believes it is best watched with a filter."
                if choice[2] == "4":
                    description += "he can't speak for or against it."


            embed = discord.Embed(
                title = choice[0],
                description = description,
                color = discord.Color.gold()
            )
            await ctx.send(embed=embed)

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.command(help = "Lists the good and okay movies that archfan has seen.")
    async def movies(self, ctx):
        try:
            embed = discord.Embed(
                title = "Movies archfan recommends",
                color = discord.Color.gold()
            )

            good_movies = [f"{x[0]}\n\n" for x in self.movies if len(x)>2 and x[2]=="1"]

            while len(good_movies) > 0:
                add = ""
                i = 0
                for good_movie in good_movies:
                    if len(add) + len(good_movie) > 1000:
                        break
                    add += good_movie
                    i += 1

                embed.add_field(
                    name = "Good As-Is",
                    value = add
                )
                good_movies = good_movies[i:]

            filter_movies = [f"{x[0]}\n\n" for x in self.movies if len(x)>2 and x[2]=="3"]

            while len(filter_movies) > 0:
                add = ""
                i = 0
                for filter_movie in filter_movies:
                    if len(add) + len(good_movie) > 1000:
                        break
                    add += filter_movie
                    i += 1

                embed.add_field(
                    name = "Needs Filtering",
                    value = add
                )
                filter_movies = filter_movies[i:]

            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")

def setup(bot):
    bot.add_cog(Movies(bot))

import discord
from discord.ext import commands
import asyncio
import time
import subprocess

class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Utility"
    
    @commands.command(help = "Shows the bot's websocket latency.")
    async def ping(self, ctx):
        await ctx.send(f"Latency: `{round(ctx.bot.latency*1000)}ms`")
    
    @commands.is_owner()
    @commands.command(help = "Evaluates Python code.", name = "eval")
    async def evaluate(self, ctx, *, code):
        try:
            result = eval(code)
            await ctx.send(result)
        except Exception as e:
            await ctx.send(str(e))
    
    @commands.is_owner()
    @commands.command(help = "Executes via the shell.")
    async def sh(self, ctx, *, command):
        try:
            def execute(cmd):
                result = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE)
                return result.stdout.decode("utf-8")
            
            result = await self.bot.loop.run_in_executor(None, execute, command)

            await ctx.send(f"```\n{result}\n```")

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.command(help = "Times the execution of a bot's command.")
    async def timebot(self, ctx):
        try:
            def check(message):
                return ctx.channel.id == message.channel.id

            msg = await self.bot.wait_for("message", timeout=10, check=check)
            start = time.time()

            reply = await self.bot.wait_for("message", timeout=10, check=check)
            finish = time.time()

            await ctx.send(f"Timed execution was {round((finish-start)*1000,2)}ms")

        except Exception as e:
            if type(e) is asyncio.TimeoutError:
                await ctx.send("Execution took too long.")
            else:
                await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.is_owner()
    @commands.command(help = "Uploads the given file to Discord for inspection.")
    async def inspect(self, ctx, path):
        try:
            upload_file = discord.File(path)
            await ctx.send(file=upload_file)
        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")
    
    @commands.command(help = "Sends the link to a custom emoji.")
    async def emoji(self, ctx, emoji : discord.PartialEmoji):
        await ctx.send(emoji.url)

def setup(bot):
    bot.add_cog(Utility(bot))

import discord
from discord.ext import commands
import json
import random
import asyncio

class Languages(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        with open("languages.json", "r") as f:
            self.languages = json.load(f)
    

    @commands.command(help="Vocabulary test in a given language, for a set number of words.")
    async def vocab(self, ctx, language, number:int=0):
        try:
            language = language.lower()

            if language == "list":
                langs = "\n".join([f"{lang.title()} ({len(list(self.languages[lang].keys()))} words)" for lang in self.languages.keys()])
                await ctx.send(f"Vocabulary sets:\n{langs}")
                return

            if language not in self.languages.keys():
                await ctx.send(f"Practice set not found: `{language}`\nSee `{ctx.prefix}vocab list` for available sets.")
                return
            
            vocab = self.languages[language]
            words = list(vocab.keys())
            if number == 0:
                number = len(words)

            if number > len(words) or number < 0:
                await ctx.send(f"Invalid number! Acceptable range is 0 to {len(words)}.")
                return

            words = words[0:number]

            random.shuffle(words)

            await ctx.send(f"Vocabulary practice: Type the definition of the word, or `stop` to stop.\n*Using a set of {number} words.*")

            def check(message):
                return message.author.id == ctx.author.id and message.channel.id == ctx.channel.id

            correct = 0
            done = 0

            for word in words:
                await asyncio.sleep(1)
                await ctx.reply(f"**{word}** `({done+1}/{number})`", mention_author=False)
                try:
                    message = (await self.bot.wait_for(event="message", timeout=60, check=check)).content.lower()
                except Exception as e:
                    await ctx.send("Took too long.")
                    return
                if message == "stop":
                    await ctx.send("Stopping.")
                    break
                answers = vocab[word].split("/")
                if message in answers:
                    await ctx.send("Correct!")
                    correct += 1
                    done += 1
                    continue
                else:
                    await ctx.send(f"Incorrect. Correct answer is `{vocab[word]}`.")
                    done += 1
                    continue
            
            embed = discord.Embed(
                title = "Review complete!",
                description = f"Vocabulary set: `{language.title()}`\nAccuracy: {correct}/{done} words ({round((correct/done)*100)}%)",
                color = discord.Color.gold()
            )
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.send(f"An exception occurred. \n```\n{e}\n```")


def setup(bot):
    bot.add_cog(Languages(bot))
            
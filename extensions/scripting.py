import discord
from discord.ext import commands

class ScriptingCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.max_commands = 10
    
    @commands.command(help="Run multiple commands in sequence")
    async def run(self, ctx, *, commands):
        message = ctx.message
        num_commands = 0
        for command in [f"{ctx.prefix}{command.strip()}" for command in commands.split("\n")]:
            if command.startswith("```"):
                continue
            if num_commands >= self.max_commands:
                await ctx.send(f"Execution halted due to number of commands (limit {self.max_commands})")
                return
            silence_output = False
            if command.endswith(";"):
                silence_output = True
                command = command[:-1]
            message.content = command
            new_ctx = await self.bot.get_context(message)

            if silence_output:
                async def fake_send(content=None, embed=None):
                    pass
                new_ctx.send = fake_send

            await self.bot.invoke(new_ctx)
            num_commands += 1
    
    # Additional commands intended for scripting

    @commands.command(help="Echoes text back at you")
    async def echo(self, ctx, *, text):
        await ctx.send(f"{ctx.author.mention} {text}")

def setup(bot):
    bot.add_cog(ScriptingCog(bot))
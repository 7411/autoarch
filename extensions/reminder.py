import discord
from discord.ext import (commands, tasks)
import asyncio
import asyncpg
import secret
import time
from dateparser.search import search_dates

poll = 10

class Reminder(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.conn = None
        if not self.check_reminders.is_running():
            self.check_reminders.start()

    async def debug(self, issue):
        id = 938887460056858684
        channel = self.bot.get_channel(id)
        if not channel:
            channel = await self.bot.fetch_channel(id)
        await channel.send(issue)

    # Ensures that the DB connection is open.
    async def check(self):
        if self.conn is None:
            self.conn = await asyncpg.connect(f'postgresql://postgres:{secret.password}@localhost')
        
    @tasks.loop(seconds=poll)
    async def check_reminders(self):
        try:
            await self.check()
            current_time = int(time.time())
            end_time = current_time + poll
            reminders = await self.conn.fetch("SELECT * FROM reminder WHERE timestamp <= $1;", end_time)
            await self.conn.execute("DELETE FROM reminder WHERE timestamp <= $1;", end_time)
            def key(reminder):
                return reminder["timestamp"]
            
            reminders.sort(key=key)

            for reminder in reminders:
                timestamp = int(reminder["timestamp"])
                user_id = reminder["user_id"]
                channel_id = reminder["channel_id"]
                reason = reminder["reason"]

                channel = self.bot.get_channel(channel_id)
                if not channel:
                    channel = await self.bot.fetch_channel(channel_id)

                current_time = time.time()
                delta_time = max(0, timestamp-current_time)
                asyncio.sleep(delta_time)
                await channel.send(f"<@{user_id}>, you asked me to remind you: {reason}")
        except Exception as e:
            await self.debug(str(e))
    
    @commands.command(help="Set a reminder for the future.")
    async def remindme(self, ctx, *, reminder):
        try:
            await self.check()
            timestamps = search_dates(reminder)
            if not timestamps:
                await ctx.send("I couldn't find a time in your reminder!\nTry specifying a time like `in an hour` or `12:00 PM EDT`.")
                return
            target_timestamp = int(timestamps[0][1].timestamp())

            current_time = time.time()
            if target_timestamp < current_time:
                await ctx.send(f"You cannot set a reminder for <t:{target_timestamp}:R>, as that is in the past.")
                return

            reason = reminder.replace(timestamps[0][0], "").strip()

            user_id = ctx.author.id
            channel_id = ctx.channel.id

            await self.conn.execute(
                "INSERT INTO reminder(timestamp, user_id, channel_id, reason) VALUES($1, $2, $3, $4);", 
                target_timestamp,
                user_id,
                channel_id,
                reason
            )

            await ctx.send(f"Alright, I'll remind you <t:{target_timestamp}:R> {reason}")

        except Exception as e:
            await ctx.send(f"An error occurred:\n```\n{e}\n```")

def setup(bot):
    bot.add_cog(Reminder(bot))
### This file contains the words that AutoArch will filter for if enabled, by category 
# (Crass Expletives, Crude Expletives, or Religiously Objectionable)






# As such, space is here added at the beginning of the file. It is highly recommended that you NOT scroll down.

















































# I am being serious about this.




























































































# Religiously Objectionable language. Not typed myself, copy/pasted from various online filters.
religious = [
    "godammit",
    "goddamit",
    "goddamn",
    "goddamned",
    "goddamnes",
    "goddamnit",
    "oh my god",
    "ohmygod",
]

# Crass language. Also copied as per above. This category largely deals with expletives related to excrement.

crass = [
    "shit",
]

# Crude language. Also copied as per above.

crude = [
    "fuck",
    "bitch",
    "whore",
    "slut",
    "boob",
    "penis",
    "vagina",
    "wtf",
    "damn",
    "dammit",
    "cunt",
    "clit",
    "sex",
    "bastard",
]
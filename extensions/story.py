import discord
from discord.ext import commands
import random

class Story(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.vowels = ["a", "e", "i", "o", "u"]
        self.consonants = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "th", "ch"]

        self.story_titles = [
            "$1 and the Curse of $2", 
            "$1's Tale", 
            "The Legend of King $1", 
            "$1 and $2: A Glorious Tale",
            "To $1 and Beyond",
            "The Many Stories of $1 the $2",
            "The Daring Adventures of $1 and $2",
            "$1's Journeys in $2land",
            "The Thrilling Epic of $1",
            "$1land: A Dystopia"
            ]

    async def get_name(self):
        length = random.randint(3, 7)

        vowel = random.choice([True, False])

        result = ""

        for i in range(length):
            if vowel:
                result += random.choice(self.vowels)
            else:
                result += random.choice(self.consonants)
            vowel = not vowel
        
        return result.capitalize()
    
    @commands.command(help = "Generates a random name using simple rules.")
    async def namegen(self, ctx):
        await ctx.send(await self.get_name())
    
    @commands.command(help = "Generates a story title using random names.")
    async def story(self, ctx):
        names = [await self.get_name() for i in range(0,10)]
        story = random.choice(self.story_titles)
        for i, name in enumerate(names):
            story = story.replace(f"${i}", name)
        
        await ctx.send(story)

def setup(bot):
    bot.add_cog(Story(bot))
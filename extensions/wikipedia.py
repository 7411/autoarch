import discord
from discord.ext import commands

class Wikipedia(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Wikipedia"

def setup(bot):
    bot.add_cog(Wikipedia(bot))